# OOP Turorial #1: Class variable
import oop_module as oop
from oop_module import Employee

#emp_1 = Employee();
#emp_2 = Employee();


#print(emp_1)
#print(emp_2)

#emp_1.first = 'Corey'
#emp_1.last = 'Schafer'
#emp_1.email = 'Corey.Schafer@company.com'
#emp_1.pay = 50000

#emp_2.first = 'Test'
#emp_2.last = 'User'
#emp_2.email = 'Test.User@company.com'
#emp_2.pay = 50000

#print(emp_1.email)
#print(emp_2.email)

emp_1 = Employee('Corey', 'Schafer', 5000)
emp_2 = Employee('Test', 'User', 6000)
print(emp_1.email)
print(emp_2.email)
print(emp_1.fullname())
print(emp_2.fullname())

print(Employee.fullname(emp_1))




