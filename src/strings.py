#tutorial 2, stings- working with Textual Data

message = 'Hello world'

print(message)

message = "Boby's world"
print (message)

message = """Bobby's world was 
ajfdljad;lfa
af;ajf;ajkdf
afja;kfj;a"""

print(message)


message = 'hello world'
print(len(message)) # Len of the message.
print(message[0])
print(message[0:5]) # Just printing hello
print(message[:5]) # Also printing hello
print(message[6:]) # print world

print(message.lower())
print(message.upper())
print(message.count('l'))
print(message.count('hello'))
print(message.find('world'))
print(message.find('universe'))

message = message.replace('world', 'universe')

print(message)

greeting = 'Hello'
name = 'Marc'

message = greeting  + ', ' + name +'. Welcome!'
print(message)

message = '{}, {}. Welcome!'.format(greeting, name)
print(message)

message = ''
message = f'{greeting}, {name.upper()}. Welcome!'
print(message)


print(dir(name)) # show method and attribute you can use.
print(help(str)) # show help for string.
print(help(str.lower))









