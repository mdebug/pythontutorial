#Tutorial #6 conditional

if True:
    print('conditional was True')

language = 'Python'

if language == 'Python':
    print('Conditional was True.')


language = 'Java'

if language == 'Python':
    print('language is Python')
elif language == 'Java':
    print('Language is Java')
elif language == 'JavaScript':
    print('Language is JavaScript')
else:
    print('no match')


# and, or, not

user = 'Admin'
logged_in = True

if not logged_in:
    print('Please log in')
else
    print("Welcome!")

if user == 'Admin' and logged_in:
    print('Admin page')
else:
    print('Bad Credentials')


a = [1, 2 ,3]
b = [1, 2, 3]

print(id(a))
print(id(b))
print(a == b)
print(a is b) # false not same object in memory.

condition = False
if condition:
    print('Evaluated to True')
else:
    print('Evalueated to False')

condition = None
if condition:
    print('Evaluated to True')
else:
    print('Evalueated to False')

condition = 0
if condition:
    print('Evaluated to True')
else:
    print('Evalueated to False')

condition = [] # '', (), [], {}
if condition:
    print('Evaluated to True')
else:
    print('Evalueated to False')


condition = 'Test'
if condition:
    print('Evaluated to True')
else:
    print('Evalueated to False')
