# Opp tutorial #4: heritence

import datetime
from oop_module import Employee
from oop_module import Developer
from oop_module import Manager


dev_1 = Developer('Corey', 'Schafer', 50000, 'Python')
dev_2 = Developer('Test', 'User', 60000, 'Java')

print(dev_1.fullname())
print(dev_1.email)
print(dev_1.prog_lang)
print(dev_2.fullname())
print(dev_2.email)
print(dev_2.prog_lang)



print(help(Developer))


print(dev_1.pay)
dev_1.apply_raise()
print(dev_2.pay)


mgr_1 = Manager('Sue', 'Smith', 90000, [dev_1])
print(mgr_1.fullname())
print(mgr_1.email)
mgr_1.print_emps()

mgr_1.add_emp(dev_2)
mgr_1.print_emps()
mgr_1.remove_emp(dev_1)
mgr_1.print_emps()


print(isinstance(mgr_1, Manager))
print(isinstance(mgr_1, Employee))
print(isinstance(mgr_1, Developer))
print(issubclass(Developer, Employee))
print(issubclass(Manager, Employee))
print(issubclass(Manager, Developer))




