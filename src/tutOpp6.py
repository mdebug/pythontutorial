#OOp tutorial #5: getter  and setter


from oop_module import Employee

emp_1 = Employee('John', 'Smith',0)



print(emp_1.first)
print(emp_1.email)
print(emp_1.fullname)


emp_1.fullname = 'Corey Schafer'
print(emp_1.first)
print(emp_1.email)
print(emp_1.fullname)

del emp_1.fullname
print(emp_1.first)
print(emp_1.email)
print(emp_1.fullname)
