#tutorial #4 list, Tuples and sets

#list
courses = ['history', "Math", 'Physics', "CompSci"]

print(courses)
print(len(courses))

print(courses[0])
print(courses[3])
print(courses[-1])

print(courses[0:2])
print(courses[:2])
print(courses[2:])
print(courses[-2:])

#courses.append('Art')
#print(courses)

courses.insert(0,'Art')

print(courses)

courses = ['history', "Math", 'Physics', "CompSci"]
courses_2 = ['Art', 'Education']
courses.extend(courses_2)
print(courses)

#courses.remove('Math')
popped = courses.pop() # remove last values.
print(popped)
print(courses)

courses = ['history', "Math", 'Physics', "CompSci"]

courses.reverse()
courses.sort()

nums = [1, 5, 2, 4, 3]
print(nums)
nums.sort()
print(nums)

courses.sort(reverse = True)
print(courses)
nums.sort(reverse = True)
print(nums)

courses = ['history', "Math", 'Physics', "CompSci"]
sorted_courses  = sorted((courses))
print(courses)
print(sorted_courses)

print(min(nums))
print(max(nums))
print(sum(nums))

courses = ['history', "Math", 'Physics', "CompSci"]
print(courses)
print(courses.index('CompSci'))

print('Art' in courses)
print('Math' in courses)

for idx, course in enumerate(courses, start =1):
    print(f'{idx} - {course}')

courses = ['history', "Math", 'Physics', "CompSci"]

course_str = ' - '.join(courses)

print(course_str)

new_list = course_str.split(' - ')
print(new_list)


# Tuples, can't modify tuple
#mutable
list_1 = ['history', "Math", 'Physics', "CompSci"]
list_2 = list_1
print(list_1)
print(list_2)

list_1[0] = 'Art'
print(list_1)
print(list_2) #list one and list 2 are changed

#Immutable
tuple_1 = ('History', 'Math', 'Physics', 'CompSci')
tuple_2 = tuple_1
print (tuple_1)
print(tuple_2)

tuple_1[0] = 'Art' # error

print (tuple_1)
print(tuple_2) #tuple 2 is

#Set no order and no duplicate
cs_courses = {'History', 'Math', 'Physics', 'CompSci', 'Math'}
print(cs_courses)
print('Math' in cs_courses)

art_courses = {'History', 'Math', 'Art', 'Design'}

print(cs_courses.intersection((art_courses)))

print(cs_courses.difference((art_courses)))
print(cs_courses.union(art_courses))


#empty
empty_list = []
empty_list = list()

empty_tuple = ()
empty_typle = tuple()

empty_set = {} # this create a dict

empty_set = set()
