#tutorial #5 dict


student = {'name': 'John', 'age': 25, 'courses': ['Math', 'CompSci']}

print(student)

print(student['name'])
print(student['courses'])

#print(student['phone']) return error

print(student.get('name'))
print(student.get('phone'))
print(student.get('phone', 'not found'))

student['phone'] = '555-5555'

student['name'] = 'Jane'
print(student)

student.update({'name':'Jane', 'age':26,  'phone': '555-5555'})

print(student)
student = {'name': 'John', 'age': 25, 'courses': ['Math', 'CompSci']}
del student['age']
print(student)
student = {'name': 'John', 'age': 25, 'courses': ['Math', 'CompSci']}
age = student.pop('age')
print(student)
print(age)

student = {'name': 'John', 'age': 25, 'courses': ['Math', 'CompSci']}
print(len(student))
print(student.keys())
print(student.values())
print(student.items())

for key in student:
    print(key)

for key, value in student.items():
    print(key, value)
