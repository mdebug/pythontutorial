#OOP tutorial #2 class variable
from oop_module import Employee
emp_1 = Employee('Corey', 'Schafer', 5000)
emp_2 = Employee('Test', 'User', 6000)
print(emp_1.email)
print(emp_2.email)
print(emp_1.fullname())
print(emp_2.fullname())

print(emp_1.pay)
emp_1.apply_raise()
print(emp_1.pay)

emp_1.raise_amount = 1.05



print (Employee.raise_amount)
print( emp_1.raise_amount)
print( emp_2.raise_amount)


print(emp_1.__dict__)
print(Employee.__dict__)

print(Employee.num_of_emps)


