#Opp Tutorial #5: special method

from oop_module import Employee
from oop_module import Developer
from oop_module import Manager

emp_1 = Employee('Corey', 'Schafer', 50000)
emp_2 = Employee('Test', 'User', 60000)

print(emp_1)

print(repr(emp_1))
print(str(emp_1))


print (emp_1 + emp_2)

print(len(emp_1))

