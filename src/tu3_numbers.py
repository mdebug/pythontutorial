#Tutorial #3 number data (integer and float)

num = 3
print(type(num))

num = 3.14
print(type(num))


print(3 +2)
print(3-2)
print(3*2)
print(3/2)
print(3//2) # floor
print( 3** 2) #power

print(3 % 2) # reminder


print(3*2+1)
print(3 * (2+1))

num = 1

num +=1
print(num)

print( abs(-3))

print( round(3.75, 1))


print( 3 == 2)
print( 3 !=2)
print( 3 >2)
print( 3 <2 )
print(3 >= 2)
print(3 <= 2)

num_1 = '100'
num_2 = '200'
num_1 = int(num_1)
num_2 = int(num_2)
print(num_1 + num_2)

